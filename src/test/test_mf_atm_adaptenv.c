/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * test_mf_atm_adaptenv.c
 *
 *  Created on: Jul 22, 2010
 *      Author: M. Barden, S. Noll
 */

extern int n_code;
extern double t_code;

#include <mf_atm.h>

int main(void) {
    double gelev = 2.5,
           gpres = 750.,
           gtemp = 250.,
           ghum = 10000,
           emix = 5;

    cpl_table *profile = NULL;

    cpl_errorstate err_state = cpl_errorstate_get();

    char filename[MF_MAXLEN] = "../../data/profiles/mipas/equ.atm";

    cpl_init(CPL_INIT_DEFAULT);

    err_state = cpl_errorstate_get();

    profile = cpl_table_new(1);

    if (mf_atm_readatm(profile, filename) != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_readatm() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
        cpl_table_delete(profile);
        cpl_end();
        return EXIT_FAILURE;
    } else {
        cpl_msg_info("mf_atm_readatm()", "Reading %s: SUCCESS", filename);
    }

    mf_atm_adaptenv_basic(profile, gelev, gpres, gtemp-273.15, ghum, emix);
    if (cpl_errorstate_is_equal(err_state)) {
        cpl_msg_info("mf_atm_adaptenv_basic()", "Profile adaption: SUCCESS");
    } else {
        cpl_msg_error(cpl_func, "mf_atm_adaptenv_basic() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
        cpl_table_delete(profile);
        cpl_end();
        return EXIT_FAILURE;
    }

    cpl_table_delete(profile);

    cpl_end();

    return EXIT_SUCCESS;

}
