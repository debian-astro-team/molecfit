/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * test_mf_atm_writeatm.c
 *
 *  Created on: Jul 21, 2010
 *      Author: barden
 */

extern int n_code;
extern double t_code;

#include <mf_atm.h>

int main(void) {
    cpl_errorstate err_state;

    cpl_table *atm_profile = NULL;         /* table with ATM profile */

    char file_in[] = "../../data/profiles/mipas/equ.atm";
    char file_out[] = "output/equ_test.atm";

    cpl_init(CPL_INIT_DEFAULT);

    err_state = cpl_errorstate_get();

    cpl_msg_set_time_on();
    cpl_msg_set_component_on();
    cpl_msg_set_domain("test_mf_atm_readatm");
    cpl_msg_set_domain_on();
    cpl_msg_set_level(CPL_MSG_INFO);
    cpl_msg_set_log_level(CPL_MSG_DEBUG);

    atm_profile = cpl_table_new(1);

    if (mf_atm_readatm(atm_profile, file_in) != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_readatm() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
    } else {
        cpl_msg_info("mf_atm_readatm()", "Reading %s: SUCCESS", file_in);
    }

    cpl_table* outprof = NULL;
    if (mf_atm_writeatm(atm_profile, &outprof, file_out) != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_writeatm() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
    } else {
        cpl_msg_info("mf_atm_writeatm()", "Writing %s: SUCCESS", file_out);
    }

    if (outprof) {
        cpl_table_delete(outprof);
    }

    cpl_table_delete(atm_profile);

    cpl_end();

    return cpl_error_get_code() ? EXIT_FAILURE : EXIT_SUCCESS;
}
