DATA FILE:
src/test/input/crires_spec_jitter_extracted_0000.fits

MPFIT RESULTS:
Status:                    2
Fit parameters:            38
Data points:               4096
Weight > 0:                3896
Frac. of valid model pix.: 1.00
Iterations:                19
Function evaluations:      350
Fit run time in min:       2.53
Avg. LBLRTM/RFM time in s: 3.70
LBLRTM/RFM calls:          32
Initial chi2:              1.476e+09
Best chi2:                 1.518e+06
Reduced chi2:              3.896e+02
RMS rel. to error:         1.974e+01
RMS rel. to mean:          3.629e-02

BEST-FIT PARAMETERS:

SPECTRAL RESOLUTION:
Rel. FWHM of boxcar (slit width = 1): 0.000 +- 0.000
FWHM of boxcar in pixels:             0.000 +- 0.000
FWHM of Gaussian in pixels:           1.952 +- 0.003
FWHM of Lorentzian in pixels:         0.500 +- 0.000

WAVELENGTH SOLUTION:
Chip 1, coef 0: -4.691e-03 +- 3.396e-06
Chip 1, coef 1:  1.001e+00 +- 5.702e-06
Chip 1, coef 2:  1.655e-03 +- 5.882e-06
Chip 1, coef 3:  1.474e-04 +- 5.172e-06
Chip 2, coef 0:  2.459e-03 +- 3.734e-06
Chip 2, coef 1:  1.005e+00 +- 8.524e-06
Chip 2, coef 2:  5.931e-03 +- 5.502e-06
Chip 2, coef 3:  4.040e-04 +- 5.692e-06
Chip 3, coef 0: -4.063e-04 +- 7.415e-06
Chip 3, coef 1:  9.996e-01 +- 1.717e-05
Chip 3, coef 2:  1.458e-03 +- 8.784e-06
Chip 3, coef 3: -5.511e-05 +- 8.249e-06
Chip 4, coef 0: -1.427e-03 +- 3.931e-06
Chip 4, coef 1:  1.001e+00 +- 7.558e-06
Chip 4, coef 2: -8.060e-04 +- 6.048e-06
Chip 4, coef 3:  1.920e-04 +- 6.415e-06

CONTINUUM CORRECTION:
Range 1, chip 1, coef 0:  4.389e+01 +- 3.216e-03
Range 1, chip 1, coef 1: -2.030e+00 +- 1.066e+00
Range 1, chip 1, coef 2: -6.904e+03 +- 9.656e+01
Range 1, chip 1, coef 3: -9.465e+04 +- 2.493e+04
Range 1, chip 2, coef 0:  4.354e+01 +- 3.829e-03
Range 1, chip 2, coef 1:  1.157e+02 +- 1.357e+00
Range 1, chip 2, coef 2:  1.677e+04 +- 1.482e+02
Range 1, chip 2, coef 3:  4.320e+06 +- 3.476e+04
Range 1, chip 3, coef 0:  3.443e+01 +- 6.616e-03
Range 1, chip 3, coef 1: -9.107e+01 +- 2.050e+00
Range 1, chip 3, coef 2:  1.530e+05 +- 5.844e+02
Range 1, chip 3, coef 3: -1.725e+07 +- 8.137e+04
Range 1, chip 4, coef 0:  3.208e+01 +- 2.972e-03
Range 1, chip 4, coef 1:  5.357e+01 +- 1.075e+00
Range 1, chip 4, coef 2:  8.312e+03 +- 1.231e+02
Range 1, chip 4, coef 3:  1.354e+04 +- 3.290e+04

RELATIVE MOLECULAR GAS COLUMNS:
H2O: 0.156 +- 0.000
CH4: 0.939 +- 0.000
 O3: 0.965 +- 0.001

MOLECULAR GAS COLUMNS IN PPMV:
H2O: 2.002e+02 +- 3.692e-02
CH4: 1.628e+00 +- 3.609e-04
 O3: 4.042e-01 +- 4.951e-04

H2O COLUMN IN MM: 0.968 +- 0.000

