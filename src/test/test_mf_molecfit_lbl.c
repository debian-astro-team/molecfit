/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/**@{*/

/*
 * test_mf_molecfit_lbl.c
 *
 * Authors:     Stefan Noll, Marco Barden, & ESO In-Kind Team Innsbruck
 * Created:     08 Nov 2010
 * Last update: 25 Sep 2012
 *
 * Test programme for MOLECFIT (batch mode).
 * LBLRTM is used as radiative transfer code.
 */


/*****************************************************************************
 *                                 INCLUDES                                  *
 ****************************************************************************/

#include <mf_molecfit.h>


/*****************************************************************************
 *                                  CODE                                     *
 ****************************************************************************/

int main(void)
{
    cpl_errorstate errstate = cpl_errorstate_get();
    char parfile[] = "config/molecfit_test_crires.par";
    char mode = 'm';
    double cs, ce;

    cpl_init(CPL_INIT_DEFAULT);

    /* Print name of parameter file */
    cpl_msg_info(cpl_func, "Driver file: %s\n", parfile);

    /* Fit data and get run time */
    cs = cpl_test_get_walltime();
    mf_molecfit(parfile, mode);
    ce = cpl_test_get_walltime();
    printf("run time: %g min\n", (ce - cs) / 60);

    /* Show errors */
    cpl_errorstate_dump(errstate, CPL_TRUE, cpl_errorstate_dump_one);

    if (cpl_errorstate_is_equal(errstate)) {
        cpl_end();
        return EXIT_SUCCESS;
    } else {
        cpl_end();
        return EXIT_FAILURE;
    }
}

/**@}*/
