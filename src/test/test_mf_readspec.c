/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * test_mf_readspec.c
 *
 * Authors:     Stefan Noll & ESO In-Kind Team Innsbruck
 * Created:     22 Jun 2010
 * Last update: 25 Sep 2012
 *
 * Test programme for mf_readspec.c
 */

#include <mf_readspec.h>

int main(void)
{
    FILE *stream;
    cpl_errorstate prev_state = cpl_errorstate_get();
    cpl_table *spec;
    mfdrv drvpar;
    char parfile[MF_LENLINE+2] = "config/molecfit_test_crires.par";
    char outfile[MF_LENLINE+2] = "output/spec.dat";
    double cs, ce;

    cpl_init(CPL_INIT_DEFAULT);

    cs = cpl_test_get_cputime();

    /* Read MOLECFIT driver file */
    mf_par_initall(&drvpar);
    mf_par_readfile(&drvpar, parfile, 0);

    /* Read spectral and header data from data file */
    spec = cpl_table_new(0);
    mf_readspec(spec, &drvpar);

    /* Write content of CPL table "spec" to file in "output/" */
    stream = fopen(outfile, "w+");
    cpl_table_dump(spec, 0, cpl_table_get_nrow(spec), stream);
    fclose(stream);

    /* Write updated driver file parameters to file in "output/" */
    mf_par_writefile(&drvpar, parfile);

    /* Free allocated memory */
    cpl_table_delete(spec);
    mf_par_deleteall(&drvpar);

    /* Show errors */
    cpl_errorstate_dump(prev_state, CPL_FALSE, cpl_errorstate_dump_one);

    ce = cpl_test_get_cputime();
    printf("run time: %g s\n", ce - cs);

    if (cpl_errorstate_is_equal(prev_state)) {
        cpl_end();
        return EXIT_SUCCESS;
    } else {
        cpl_end();
        return EXIT_FAILURE;
    }
}
