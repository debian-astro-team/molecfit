/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/**@{*/

/*
 * calc_pwv.c
 *
 * Authors:     Marco Barden, Stefan Noll, & ESO In-Kind Team Innsbruck
 * Created:     09 Dec 2011
 * Last update: 30 Oct 2013
 *
 * Calculate PWV values for atm-file
 */


/*****************************************************************************
 *                                 INCLUDES                                  *
 ****************************************************************************/

#include <mf_mpfit.h>


/*****************************************************************************
 *                                GLOBALS                                    *
 ****************************************************************************/

/* Definition of global variables */

extern int nfev;
extern int n_code;
extern double t_code;


/*****************************************************************************
 *                                  CODE                                     *
 ****************************************************************************/

int main(int argc, char *argv[]) {
    // syntax: calc_pwv "atmfile" "gdasfile"
    // gdasfile includes MeteoMonitor correction!

    FILE *fp;

    int i;

    cpl_error_code err_code;

    double pwv = 0.;

    const double geoelev = 2.635;
    char atmfile[MF_MAXLEN];
    // = "/home/barden/Projects/ESO-inkind/C/DR05_Mod1/data/equ.atm";
    char gdasfile[MF_MAXLEN];
    // = "/home/barden/Projects/ESO-inkind/C/DR05_Mod1/data/GDAS_t0_s0.atm";
    char outfile[MF_MAXLEN];

    cpl_table *atm, *gdas, *prof;

    cpl_array *molecs;


    if (argc < 3) {
        return EXIT_FAILURE;
    }

    cpl_init(CPL_INIT_DEFAULT);

    sprintf(atmfile, "%s", argv[1]);
    sprintf(gdasfile, "%s", argv[2]);

    /* Read GDAS profile from file */
    gdas = cpl_table_new(0);
    if (mf_atm_readgdas(gdas, gdasfile, "km") != CPL_ERROR_NONE) {
        cpl_table_delete(gdas);
        cpl_end();
        return cpl_error_set_message(cpl_func, CPL_ERROR_FILE_NOT_FOUND,
                                     "Could not read GDAS profile: %s",
                                     gdasfile);
    }


    /* Read standard profile from file */
    atm = cpl_table_new(0);
    if (mf_atm_readatm(atm, atmfile) != CPL_ERROR_NONE) {
        atm = cpl_table_duplicate(gdas);
    }

    /* merge GDAS and standard profiles */
    molecs = cpl_array_new(1, CPL_TYPE_STRING);
    cpl_array_set_string(molecs, 0, "H2O");

    prof = cpl_table_new(0);

    err_code = mf_atm_convertgdas_fixed(atm, gdas, prof, molecs, geoelev);
    err_code = CPL_ERROR_NONE;

    if (err_code != CPL_ERROR_NONE) {
        cpl_array_delete(molecs);
        cpl_table_delete(atm);
        cpl_table_delete(gdas);
        cpl_table_delete(prof);
        cpl_end();
        return cpl_error_set_message(cpl_func, cpl_error_get_code(),
                                     "Could not merge profiles");
    }

    if (argc == 4) {
        sprintf(outfile, "%s", argv[3]);
        fp = fopen(outfile, "w");

        for (i = 0; i < cpl_table_get_nrow(prof)-1; i++) {
            fprintf(fp, "%f %f %f %f\n",
                    cpl_table_get_float(prof, "PRE", i, NULL),
                    cpl_table_get_float(prof, "HGT", i, NULL),
                    cpl_table_get_float(prof, "TEM", i, NULL),
                    cpl_table_get_float(prof, "H2O", i, NULL));
        }
    }

    mf_atm_calpwv(&pwv, prof, &geoelev);

    cpl_msg_info(cpl_func, "PWV: %lf", pwv);

    cpl_array_delete(molecs);
    cpl_table_delete(atm);
    cpl_table_delete(gdas);
    cpl_table_delete(prof);

    cpl_end();

    return EXIT_SUCCESS;
}

/**@}*/
