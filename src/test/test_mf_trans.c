/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/**@{*/

/*
 * test_mf_trans.c
 *
 * Authors:     Stefan Noll & ESO In-Kind Team Innsbruck
 * Created:     26 Sep 2012
 * Last update: 28 Aug 2013
 *
 * Test programme for CALCTRANS
 */


/*****************************************************************************
 *                                 INCLUDES                                  *
 ****************************************************************************/

#include <mf_trans.h>


/*****************************************************************************
 *                                  CODE                                     *
 ****************************************************************************/

int main(void)
{
    cpl_errorstate errstate = cpl_errorstate_get();
    char name[] = "molecfit_test_crires";
    char parfile[MF_MAXLEN] = "", sys[MF_MAXLEN] = "";
    double cs, ce;

    cpl_init(CPL_INIT_DEFAULT);

    /* Print name of parameter file */
    sprintf(parfile, "config/%s.par", name);
    cpl_msg_info(cpl_func, "Driver file: %s\n", parfile);

    /* Copy required input files to output directory */
    sprintf(sys, "cp input/%s.fits output/", name);
    if (system(sys)) {};
    sprintf(sys, "cp input/%s_fit.atm output/", name);
    if (system(sys)) {};
    sprintf(sys, "cp input/%s_fit.res output/", name);
    if (system(sys)) {};

    /* Calculate transmission curve and get run time */
    cs = cpl_test_get_walltime();
    mf_trans(parfile);
    ce = cpl_test_get_walltime();
    printf("run time: %g s\n", ce - cs);

    /* Show errors */
    cpl_errorstate_dump(errstate, CPL_TRUE, cpl_errorstate_dump_one);

    if (cpl_errorstate_is_equal(errstate)) {
        cpl_end();
        return EXIT_SUCCESS;
    } else {
        cpl_end();
        return EXIT_FAILURE;
    }
}

/**@}*/
