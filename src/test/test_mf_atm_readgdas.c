/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * test_mf_atm_readgdas.c
 *
 *  Created on: Jul 5, 2010
 *      Author: barden
 */

extern int n_code;
extern double t_code;

#include <mf_atm.h>

int main(void) {
    cpl_errorstate err_state;

    cpl_table *gdas_profile = NULL;         /* table with GDAS profile */

    char filename1[MF_MAXLEN] = "input/2009-05-01T06.gdas"; // hgt in [m]
    char filename2[MF_MAXLEN] = "input/GDAS_t0_s1.atm";     // hgt in [km]

    cpl_init(CPL_INIT_DEFAULT);

    err_state = cpl_errorstate_get();

    cpl_msg_set_time_on();
    cpl_msg_set_component_on();
    cpl_msg_set_domain("test_mf_atm_readgdas");
    cpl_msg_set_domain_on();
    cpl_msg_set_level(CPL_MSG_INFO);
    cpl_msg_set_log_level(CPL_MSG_DEBUG);

    gdas_profile = cpl_table_new(1);
    if (mf_atm_readgdas(gdas_profile, filename1, "m") != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_readgdas() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
    } else {
        cpl_msg_info("mf_atm_readgdas()", "Reading %s: SUCCESS", filename1);
//        cpl_table_dump(gdas_profile, 0, cpl_table_get_nrow(gdas_profile),
//                       NULL);
    }
    cpl_table_delete(gdas_profile);

    gdas_profile = cpl_table_new(1);
    if (mf_atm_readgdas(gdas_profile, filename2, "km") != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_readgdas() failed:");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
    } else {
        cpl_msg_info("mf_atm_readgdas()", "Reading %s: SUCCESS", filename2);
//        cpl_table_dump(gdas_profile, 0, cpl_table_get_nrow(gdas_profile),
//                       NULL);
    }
    cpl_table_delete(gdas_profile);

    cpl_end();

    return cpl_error_get_code() ? EXIT_FAILURE : EXIT_SUCCESS;
}
