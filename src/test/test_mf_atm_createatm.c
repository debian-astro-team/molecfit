/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * test_mf_atm_createatm.c
 *
 *  Created on: Jul 8, 2010
 *      Author: barden
 */

extern int n_code;
extern double t_code;

#include <cpl_test.h>
#include <mf_readspec.h>
#include <mf_lblrtm.h>
#include <mf_atm.h>

int main(void) {
    cpl_errorstate err_state;

    mfdrv drvpar;
    char parfile[] = "config/molecfit_test_crires.par";

    cpl_table *atm_profile = NULL;         /* table with ATM profile */

    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    err_state = cpl_errorstate_get();

    atm_profile = cpl_table_new(0);

    mf_par_initall(&drvpar);
    if (mf_par_readfile(&drvpar, parfile, 0) != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_createatm() failed: "
                "error reading driver file: %s", parfile);
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
    } else {
        mf_readspec_header(&drvpar);
        if (mf_atm_createatm(atm_profile, &drvpar) != CPL_ERROR_NONE) {
        cpl_msg_error(cpl_func, "mf_atm_createatm() failed: "
                "error creating atmospheric profile");
        cpl_errorstate_dump(err_state, CPL_FALSE, cpl_errorstate_dump_one);
        } else {
            cpl_test_rel(cpl_table_get(atm_profile, "TEM", 0, NULL),
                         289.848, 1e-5);
            cpl_test_rel(cpl_table_get(atm_profile, "HGT", 0, NULL),
                         2., 1e-5);

            cpl_test_rel(cpl_table_get(atm_profile, "TEM", 31, NULL),
                         256.38, 1e-5);
            cpl_test_rel(cpl_table_get(atm_profile, "HGT", 31, NULL),
                         39.4286, 1e-5);
        }
    }

    mf_par_deleteall(&drvpar);
    cpl_table_delete(atm_profile);

    return cpl_test_end(0);
}
