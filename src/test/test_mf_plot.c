/*
 *  This file is part of the MOLECFIT software package.
 *  Copyright (C) 2009-2013 European Southern Observatory
 *
 *  This programme is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This programme is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this programme. If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \ingroup molecfit
 */

/**@{*/

/*!
 * \callgraph
 *
 * \file test_mf_plot.c
 *
 * test routine for plotting library
 *
 * \author Wolfgang Kausch & ESO In-Kind Team Innsbruck
 *
 */

/*****************************************************************************
 *                                 INCLUDES                                  *
 ****************************************************************************/

#include <mf_plot.h>

/*****************************************************************************
 *                                GLOBALS                                    *
 ****************************************************************************/

/* Definition of global variables */

int n_code = 0;
double t_code = 0.;

/*****************************************************************************
 *                                    CODE                                   *
 ****************************************************************************/

int main(void)
{
    FILE *specfile;
//    double cs=0, ce=0;

    char input_filename1[MF_MAXLEN],     /* spectra to be read              */
         input_filename2[MF_MAXLEN];
    char line[MF_MAXLEN];                /* line in spectrum file           */

    int loop=0,                          /* used for loops                  */
        linecount=0;                     /* for determining length of input */

    char *entry;                         /* data point in spectrum file     */
    float lambda=0;                      /* wavelength                      */
    double data_point=0;                 /* data point                      */

    char x_label[MF_MAXLEN], y_label[MF_MAXLEN], title[MF_MAXLEN];
    cpl_errorstate error_state;          /* CPL error state                 */

    /* CPL test tables */
    cpl_table *spectrum1, *spectrum2, *testdat;

    /* Reading parameter file */
    char parfile[MF_LENLINE+2] = "config/molecfit_test_crires.par";
    mfdrv drvpar;

/*--------------------------------------------------------------------------*/
    /* Initialising */
    cpl_init(CPL_INIT_DEFAULT);
    error_state = cpl_errorstate_get();
//    cs = cpl_test_get_cputime();
    cpl_msg_set_time_off();
    cpl_msg_set_component_on();
    cpl_msg_set_domain("[test_mf_plot]");
    cpl_msg_set_domain_on();
    cpl_msg_set_log_name("test_mf_plot.log");
    cpl_msg_set_level(CPL_MSG_INFO);
    cpl_msg_set_log_level(CPL_MSG_DEBUG);

/*--------------------------------------------------------------------------*/
/* single + double plot                                                     */
/*--------------------------------------------------------------------------*/

    /* Reading parameter file */
    mf_par_initall(&drvpar);
    mf_par_readfile(&drvpar, parfile, 0);

    /* reading test spectra */
    mf_basic_initstring(input_filename1,MF_MAXLEN);
    mf_basic_initstring(input_filename2,MF_MAXLEN);

    sprintf(input_filename1,"input/testspec1.dat");
    sprintf(input_filename2,"input/testspec2.dat");

    spectrum1=cpl_table_new(0);
    cpl_table_new_column(spectrum1,"LAMBDA",CPL_TYPE_DOUBLE);
    cpl_table_new_column(spectrum1,"RADIANCE",CPL_TYPE_DOUBLE);

    spectrum2=cpl_table_new(0);
    cpl_table_new_column(spectrum2,"LAMBDA",CPL_TYPE_DOUBLE);
    cpl_table_new_column(spectrum2,"RADIANCE",CPL_TYPE_DOUBLE);

    linecount=0;
    loop=0;

    /* Determining cpl table sizes */
    specfile = fopen(input_filename1,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        linecount++;
    }
    fclose(specfile);
    cpl_table_set_size(spectrum1,linecount);
    linecount=0;

    specfile = fopen(input_filename2,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        linecount++;
    }
    fclose(specfile);
    cpl_table_set_size(spectrum2,linecount);

    /* Reading spectrum 1 */
    specfile = fopen(input_filename1,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        mf_basic_terminatestring(line);
        entry = strtok(line,"\t");
        lambda=1e4/atof(entry);

        entry = strtok(NULL, "\t" );
        data_point=atof(entry);
        cpl_table_set_double(spectrum1,"LAMBDA",loop,lambda);
        cpl_table_set_double(spectrum1,"RADIANCE",loop,data_point);
        loop++;
    }
    fclose(specfile);
    loop=0;

    /* Reading spectrum 2 */
    specfile = fopen(input_filename2,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        mf_basic_terminatestring(line);
        entry = strtok(line,"\t");
        lambda=1e4/atof(entry);

        entry = strtok(NULL, "\t" );
        data_point=atof(entry);
        cpl_table_set_double(spectrum2,"LAMBDA",loop,lambda);
        cpl_table_set_double(spectrum2,"RADIANCE",loop,data_point);
        loop++;
    }

    fclose(specfile);
    sprintf(title,"DEMO: mf plot single");

/*--------------------------------------------------------------------------*/

    mf_plot_single(spectrum1,NULL,NULL,title,&drvpar);
    mf_plot_double(spectrum1,spectrum2,&drvpar);

/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/
/* xyplot                                                                   */
/*--------------------------------------------------------------------------*/

    /* Reading input data: testdata is f(x)=sin(x)/x */
    mf_basic_initstring(input_filename1,MF_MAXLEN);
    sprintf(input_filename1,"input/testdat_plot_xy.dat");

    testdat=cpl_table_new(0);
    cpl_table_new_column(testdat,"x",CPL_TYPE_DOUBLE);
    cpl_table_new_column(testdat,"y",CPL_TYPE_DOUBLE);

    linecount=0;
    loop=0;
    /* Determining cpl table sizes */
    specfile = fopen(input_filename1,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        linecount++;
    }
    fclose(specfile);
    cpl_table_set_size(testdat,linecount);

    specfile = fopen(input_filename1,"r");
    while (fgets(line, MF_MAXLEN - 1, specfile) != NULL)
    {
        mf_basic_terminatestring(line);
        entry = strtok(line,"\t");
        lambda=atof(entry);

        entry = strtok(NULL, "\t" );
        data_point=atof(entry);
        cpl_table_set_double(testdat,"x",loop,lambda);
        cpl_table_set_double(testdat,"y",loop,data_point);
        loop++;
    }

    fclose(specfile);

    sprintf(x_label,"x axis");
    sprintf(y_label,"y axis");
    sprintf(title,"DEMO: mf plot xy");

/*--------------------------------------------------------------------------*/
    mf_plot_xy(testdat,x_label,y_label,title,&drvpar);
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* Cleaning                                                                 */
/*--------------------------------------------------------------------------*/

//     cpl_table_dump(spectrum1,0,4,NULL);
//     cpl_table_dump(spectrum2,0,4,NULL);
//     cpl_table_dump(testspec,0,4,NULL);
//     cpl_table_dump(testdat,0,4,NULL);
//
//     cpl_table_dump_structure(spectrum1,NULL);
//     cpl_table_dump_structure(spectrum2,NULL);
//     cpl_table_dump_structure(testspec,NULL);
//     cpl_table_dump_structure(testdat,NULL);

    mf_par_deleteall(&drvpar);

    cpl_table_delete(spectrum1);
    cpl_table_delete(spectrum2);
    cpl_table_delete(testdat);

//     cpl_memory_dump();
//    ce = cpl_test_get_cputime();
//     printf("run time: %g s\n", ce - cs);
//     cpl_errorstate_dump(error_state, CPL_FALSE, cpl_errorstate_dump_one);

    if (cpl_errorstate_is_equal(error_state)) {
        cpl_end();
        return EXIT_SUCCESS;
    } else {
        cpl_end();
        return EXIT_FAILURE;
    }

}
