Source: molecfit
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11),
               dh-python,
               gfortran,
               libcpl-dev,
               python
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/debian-astro-team/molecfit
Vcs-Git: https://salsa.debian.org/debian-astro-team/molecfit.git
Homepage: http://www.eso.org/sci/software/pipelines/skytools/molecfit

Package: molecfit
Architecture: any
Depends: python-matplotlib,
         python-numpy,
         python-wx,
         ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Description: Correcting Observations for Telluric Absorption
 Molecfit is a software tool to correct astronomical observations for
 atmospheric absorption features, based on fitting synthetic
 transmission spectra to the astronomical data. It can also estimate
 molecular abundances, especially the water vapour content of the
 Earth’s atmosphere.

Package: libmolecfit0
Architecture: any
Section: libs
Depends: molecfit-data,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Correcting Observations for Telluric Absorption (shared library)
 Molecfit is a software tool to correct astronomical observations for
 atmospheric absorption features, based on fitting synthetic
 transmission spectra to the astronomical data. It can also estimate
 molecular abundances, especially the water vapour content of the
 Earth’s atmosphere.
 .
 This package contains the shared library.

Package: libmolecfit-dev
Architecture: any
Section: libdevel
Depends: libmolecfit0 (=  ${binary:Version}),
         ${misc:Depends}
Description: Correcting Observations for Telluric Absorption (dev files)
 Molecfit is a software tool to correct astronomical observations for
 atmospheric absorption features, based on fitting synthetic
 transmission spectra to the astronomical data. It can also estimate
 molecular abundances, especially the water vapour content of the
 Earth’s atmosphere.
 .
 This package contains the development files.

Package: molecfit-data
Architecture: all
Depends: ${misc:Depends}
Recommends: libmolecfit0
Description: Correcting Observations for Telluric Absorption (data files)
 Molecfit is a software tool to correct astronomical observations for
 atmospheric absorption features, based on fitting synthetic
 transmission spectra to the astronomical data. It can also estimate
 molecular abundances, especially the water vapour content of the
 Earth’s atmosphere.
 .
 This package contains the data files for the package

Package: molecfit-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Correcting Observations for Telluric Absorption (documentation)
 Molecfit is a software tool to correct astronomical observations for
 atmospheric absorption features, based on fitting synthetic
 transmission spectra to the astronomical data. It can also estimate
 molecular abundances, especially the water vapour content of the
 Earth’s atmosphere.
 .
 This package contains the documentation for the package
