### Driver for MOLECFIT

## INPUT DATA

# Data file name (path relative to the current directory or absolute path)
filename: __prefixexamples__/FLAMES/ngc3603_mod.fits

# ASCII list of files to be corrected for telluric absorption using the
# transmission curve derived from the input reference file (path of list and
# listed files relative to the current directory or absolute path; default: "none")
listname: none

# Type of input spectrum -- 1 = transmission (default); 0 = emission
trans: 1

# Names of the file columns (table) or extensions (image) containing:
# Wavelength  Flux  Flux_Err  Mask
# - Flux_Err and/or Mask can be avoided by writing 'NULL'
# - 'NULL' is required for Wavelength if it is given by header keywords
# - parameter list: col_lam, col_flux, col_dflux, and col_mask
columns: NULL NULL NULL NULL

# Default error relative to mean for the case that the error column is missing
default_error: 0.01

# Multiplicative factor to convert wavelength to micron
# (e.g. nm -> wlgtomicron = 1e-3)
wlgtomicron: 1e-03

# Wavelengths in vacuum (= vac) or air (= air)
vac_air: air

# ASCII or FITS table for wavelength ranges in micron to be fitted
# (path relative to the current directory or absolute path; default: "none")
wrange_include: __prefixexamples__/FLAMES/include_wl.dat

# ASCII or FITS table for wavelength ranges in micron to be excluded from the
# fit (path relative to the current directory or absolute path; default: "none")
wrange_exclude: __prefixexamples__/FLAMES/exclude_wl.dat

# ASCII or FITS table for pixel ranges to be excluded from the fit
# (path relative to the current directory or absolute path; default: "none")
prange_exclude: __prefixexamples__/FLAMES/exclude_pix.dat

## RESULTS

# Directory for output files (path relative to the current directory or absolute path)
output_dir: output

# Name for output files
# (supplemented by "_fit" or "_tac" as well as ".asc", ".atm", ".fits",
# ".par, ".ps", and ".res")
output_name: molecfit_flames

# Plot creation: gnuplot is used to create control plots
# W - screen output only (incorporating wxt terminal in gnuplot)
# X - screen output only (incorporating x11 terminal in gnuplot)
# P - postscript file labelled '<output_name>.ps', stored in <output_dir>
# combinations possible, i.e. WP, WX, XP, WXP (however, keep the order!)
# all other input: no plot creation is performed
plot_creation: none

# Create plots for individual fit ranges? -- 1 = yes; 0 = no
plot_range: 0

## FIT PRECISION

# Relative chi2 convergence criterion
ftol: 1e-06

# Relative parameter convergence criterion
xtol: 1e-06

## MOLECULAR COLUMNS

# List of molecules to be included in the model
# (default: 'H2O', N_val: nmolec)
list_molec:   H2O

# Fit flags for molecules -- 1 = yes; 0 = no (N_val: nmolec)
fit_molec:   1

# Values of molecular columns, expressed relatively to the input ATM profile
# columns (N_val: nmolec)
relcol:   1

## BACKGROUND AND CONTINUUM

# Conversion of fluxes from phot/(s*m2*mum*as2) (emission spectrum only) to
# flux unit of observed spectrum:
# 0: phot/(s*m^2*mum*as^2) [no conversion]
# 1: W/(m^2*mum*as^2)
# 2: erg/(s*cm^2*A*as^2)
# 3: mJy/as^2
# For other units the conversion factor has to be considered as constant term
# of the continuum fit.
flux_unit: 0

# Fit of telescope background -- 1 = yes; 0 = no (emission spectrum only)
fit_back: 0

# Initial value for telescope background fit (range: [0,1])
telback: 0.1

# Polynomial fit of continuum --> degree: cont_n
fit_cont: 1

# Degree of coefficients for continuum fit
cont_n: 1

# Initial constant term for continuum fit (valid for all fit ranges)
# (emission spectrum: about 1 for correct flux_unit)
cont_const: 3000.0

## WAVELENGTH SOLUTION

# Refinement of wavelength solution using a polynomial of degree wlc_n
fit_wlc: 1

# Polynomial degree of the refined wavelength solution
wlc_n: 0

# Initial constant term for wavelength correction (shift relative to half
# wavelength range)
wlc_const: -0.0

## RESOLUTION

# Fit resolution by boxcar -- 1 = yes; 0 = no
fit_res_box: 0

# Initial value for FWHM of boxcar relative to slit width (>= 0. and <= 2.)
relres_box: 0.0

# Voigt profile approximation instead of independent Gaussian and Lorentzian
# kernels? -- 1 = yes; 0 = no
kernmode: 0

# Fit resolution by Gaussian -- 1 = yes; 0 = no
fit_res_gauss: 1

# Initial value for FWHM of Gaussian in pixels
res_gauss: 1.0

# Fit resolution by Lorentzian -- 1 = yes; 0 = no
fit_res_lorentz: 0

# Initial value for FWHM of Lorentzian in pixels
res_lorentz: 0.0

# Size of Gaussian/Lorentzian/Voigtian kernel in FWHM
kernfac: 300.0

# Variable kernel (linear increase with wavelength)? -- 1 = yes; 0 = no
varkern: 0

# ASCII file for kernel elements (one per line; normalisation not required)
# instead of synthetic kernel consisting of boxcar, Gaussian, and Lorentzian
# components (path relative to the current directory or absolute path; default: "none")
kernel_file:        none
## INSTRUMENTAL PARAMETERS

# Pixel scale in arcsec (taken from this file only)
pixsc: 1.000

clean_mflux: 1
obsdate: 54272.97378
utc: 84133.000000
telalt: 50.620
rhum: 24.000
pres: 742.1
temp: 9.000000
m1temp: 8.030000
longitude: -70.405100
latitude: -24.627600
slitw: 1.000

# PWV value in mm for the input water vapour profile. The merged profile
# composed of ref_atm, GDAS, and local meteo data will be scaled to this value
# if pwv > 0 (default: -1 -> no scaling).
pwv: -1

end
