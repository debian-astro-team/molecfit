subdir_files = \
  figures/crires_RT.eps \
  figures/molecfit_xshoo_vis_1_fit.pdf \
  figures/ngt_equ.eps \
  figures/cheat_sheet_RT.eps \
  figures/equ_ngt_4-28mu_tra.eps \
  figures/visir4_RT.eps \
  figures/molecfit_VR_091024A_114_fit.pdf \
  figures/molecfit_VR_091024A_122_fit.pdf \
  figures/equ_ngt_4-28mu_rad.eps \
  figures/molecfit_VR_091118A_114_fit.pdf \
  figures/molecfit_VR_091118A_122_fit.pdf \
  figures/visir3_RT.eps \
  figures/profiles.eps \
  figures/mfd_linedensity.pdf \
  figures/molecfit_xshoo_vis_1_fit_1.pdf \
  figures/molecfit_xshoo_vis_1_fit_2.pdf \
  figures/xshooter_RT.eps \
  figures/molecfit_xshoo_tellstd_nir_fit.pdf \
  figures/gdas_wdir_height_v.eps \
  figures/mfd_tactest2.pdf \
  figures/Overview_SM-03_molecfit.eps \
  figures/meteomonitor.eps \
  figures/molecfit_xshoo_tellstd_nir_tac.pdf \
  figures/mfd_tactest1.pdf \
  figures/molecfit_workflow.eps \
  figures/molecfit_crires_fit.pdf
